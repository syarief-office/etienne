$(document).ready(function() {
    //Menu
    menu();

    function menu() {
        var iconMenu = $('.menu');
        iconMenu.click(function(e) {
            e.stopPropagation();
            var nav_xs = $('.navigation');
            nav_xs.toggleClass('open');
        });
        $('.nav ul').on("click", function(e) {
            e.stopPropagation();
        });

    }

    //Wrapper
    wrappers();

    function wrappers() {
        var windheight = $(window).height();
        var headheight = $('.app-header').outerHeight();
        var footheight = $('.app-footer').outerHeight();
        var wrap = $('.wrapper');
        wrap.css({
            //'min-height': windheight,
            //'padding-bottom': footheight - 2,
            'padding-top': headheight - 2,
            'position': 'relative'
        })
    }
    $(window).resize(function() {
        wrappers();
    });



    // Dropdown Menu Fade    
    jQuery(document).ready(function() {
        $(".dropdown").hover(function() {
            $('.dropdown-menu', this).stop().fadeIn("fast");
        }, function() {
            $('.dropdown-menu', this).stop().fadeOut("fast");
        });
    });

    // Fancybox
    $('.fancybox').fancybox();

    // Accordion
    $(".accordion-toggle.acc").click(function() {
        $(".wrap-product-list").toggleClass("closed");
    });

    //Search
    $(".src").click(function() {
        $(".find").toggleClass("open");
    });

    //checkbox
    $(".checkbox").click(function() {
        $(".pass").toggleClass("open");
    });

    //pay by card
    $(".pay").click(function() {
        $(".wrap-pay-card").toggleClass("open");
    });
    $(document).ready(function() {
        $(".payt").click(function() {
            $(".wrap-pay-card").toggleClass("open");
        });
    });
    $(document).ready(function() {
        $(".accordion-toggle").click(function() {
            $(".accordion-toggle").toggleClass("accordion-opened");
        });
    });

    // QUANTITY BUTTON
    $('.btn-number').click(function(e) {
        e.preventDefault();
        fieldName = $(this).attr('data-field');
        type = $(this).attr('data-type');
        var input = $("input[name='" + fieldName + "']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if (type == 'minus') {
                if (currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
                if (parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }
            } else if (type == 'plus') {
                if (currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if (parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }
            }
        } else {
            input.val(0);
        }
    });
    $('.input-number').focusin(function() {
        $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function() {
        minValue = parseInt($(this).attr('min'));
        maxValue = parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());
        name = $(this).attr('name');
        if (valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr(
                'disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if (valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr(
                'disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }
    });
    $(".input-number").keydown(function(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 ||
                e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    // Slides on quickview 
    $("#slider-qv").responsiveSlides({
        auto: false,
        pager: false,
        nav: true,
        speed: 500,
        namespace: "callbacks",
        before: function() {
            $('.events').append("<li>before event fired.</li>");
        },
        after: function() {
            $('.events').append("<li>after event fired.</li>");
        }
    });

    //Slides on Product detail page
    var thumbs = jQuery('#thumbnails').slippry({
        // general elements & wrapper
        slippryWrapper: '<div class="slippry_box thumbnails" />',
        // options
        transition: 'fade',
        pager: false,
        auto: false,
        onSlideBefore: function(el, index_old, index_new) {
            jQuery('.thumbs a img').removeClass('active');
            jQuery('img', jQuery('.thumbs a')[index_new]).addClass('active');
        }
    });
    jQuery('.thumbs a').click(function() {
        thumbs.goToSlide($(this).data('slide'));
        return false;
    });

    //Zoom image
    $(document).ready(function() {
        $('.ex3').zoom({
            on: 'click'
        });
    });

    //dropdown filter on product list
    $(document).ready(function() {
        $(".flip-m").click(function() {
            $(".panel-m").slideToggle("fast");
        });
    });
    $(document).ready(function() {
        $(".flip-s").click(function() {
            $(".panel-s").slideToggle("fast");
        });
    });
    $(document).ready(function() {
        $(".flip-c").click(function() {
            $(".panel-c").slideToggle("fast");
        });
    });
    $(document).ready(function() {
        $(".flip-p").click(function() {
            $(".panel-p").slideToggle("fast");
        });
    });

    //scrool filter bar on product list
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 50) {
            $(".wrap-bar-filter").css('padding-top', 5);
        } else {
            $(".wrap-bar-filter").css('padding-top', 50);
        }
    });

    $(window).scroll(function() {
        /*var scroll = $(window).scrollTop();
        if (scroll >= 1400) {
            $(".wrap-filter-sidebar").addClass("filstop");
        } else {
            $(".wrap-filter-sidebar").removeClass("filstop");
        }*/
    });

    //SCROLLING SIDEBAR
    if(jQuery('.wrap-filter-sidebar').length){
        var $window = $(window),
            $el = jQuery('.wrap-filter-sidebar'),
            $gap = jQuery('.app-footer'),
            $scroLimit = jQuery('.scroLimit'),
            stickyTop = $el.offset().top,
            fooTop = $gap.offset().top,
            elHeight = $el.innerHeight(),
            elhalfHeight = $el.height();
        jQuery(window).scroll(function(){
            var winTop = jQuery(window).scrollTop(),
                winTopPlus = jQuery(window).scrollTop()  + elHeight,
                inOutElement = elHeight -  elhalfHeight,
                limit = fooTop - elHeight - 30;
                //bottom = $scroLimit.offset().top + $scroLimit.outerHeight(true);

            if($window.width() > 767){
                if( 0 < winTop){
                    $el.css({
                        'position' : 'fixed',
                        'top' : 0,
                        'padding-top' : 215
                    }).addClass('scrolling');
                }else{
                    $el.css({
                        'position': 'fixed',
                        'padding-top':  215
                    });
                }
                if( limit < winTopPlus){
                    var diff = limit - winTop;
                    $el.css({
                        top: diff,
                        'padding-top' : inOutElement
                    });
                }
            }
        });
    }

    /*$('.wrap-filter-sidebar').sticky({
        topSpacing: 0, // Space between element and top of the viewport
        zIndex: 100, // z-index
        stopper: ".app-footer" // Id, class, or number value
    });*/

    //selectpicker disable dropup
    $('.selectpicker').selectpicker({
        dropupAuto : false
        //size : true
    });

    //Flexslider
     $(window).load(function() {
        $('.flexslider').flexslider({
            animation: "slide"
        });
    });

});
